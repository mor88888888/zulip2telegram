# zulip2telegram

Telegram BOT that send notifications from Zulip

## Description
The motivation for this project comes from the impossibility to configure notifications in Zulip for Android without GApps.

With this project you're able to forward notifications from a Zulip account to a Telegram chat.

## Installation
Create a `config.json` file in the same directory with these fields:
```
{
  "zulip_site": "https://zulip.example.com",
  "api_key": "",
  "email": "john@example.com",
  "telegram_token": "",
  "telegram_chat_id": "",
  "excluded_streams": ["spam stream", "my boss"],
  "me": "@**John Wee**"
}
```

Install requirements:
```
pip3 install -r requirements.txt
```

## Usage
```
python3 zulip2telegram.py
```

## Support
I you find any bug, you have a suggestion or you don't know how works a specific tool, let me know:

🐛 [Add issue](https://gitlab.com/mor88888888/zulip2telegram/-/issues/new)

📬 mor8@protonmail.com

## Roadmap
- `config.json` autoconfiguration.
- Add Linux service configuration example.