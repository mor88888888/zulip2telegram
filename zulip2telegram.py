#!/usr/bin/env python3

import json
import sys
from zulip import Client
import requests
import time
from datetime import datetime

def get_timestamp():
	now = datetime.now()
	return now.strftime("[%Y-%m-%d %H:%M:%S]")

def load_zulip_config(config_file):
	with open(config_file) as read_file:
		config = json.load(read_file)
	return config

zulip_config = load_zulip_config("config.json")

# Obtén las variables de configuración
ZULIP_SITE = zulip_config["zulip_site"]
ZULIP_API_KEY = zulip_config["api_key"]
ZULIP_EMAIL = zulip_config["email"]
TELEGRAM_TOKEN = zulip_config["telegram_token"]
TELEGRAM_CHAT_ID = zulip_config["telegram_chat_id"]
EXCLUDED_STREAMS = zulip_config["excluded_streams"]
ME = zulip_config["me"]

def should_send_notification(content, stream, sender):
	if ME in content:
		return True
	if ME in sender:
		return False
	return stream not in EXCLUDED_STREAMS

def forward_zulip_notifications_to_telegram():
	zulip_client = Client(site=ZULIP_SITE, api_key=ZULIP_API_KEY, email=ZULIP_EMAIL)

	# Escucha de notificaciones de Zulip
	def handle_zulip_notification(event):
		print(f"{get_timestamp()} [VERBOSE] {event}")
		content = event['content']
		sender = event['sender_full_name']
		stream = event['display_recipient']
		message_content = f"Stream: {stream}\nFrom: {sender}\n\n{content}"
		if should_send_notification(content, stream, sender):
			send_telegram_message(message_content)

	# Registro del controlador de notificaciones de Zulip
	zulip_client.call_on_each_message(handle_zulip_notification)

	# Mantener el script en ejecución
	zulip_client.run_forever()

def send_telegram_message(message):
	url = f"https://api.telegram.org/bot{TELEGRAM_TOKEN}/sendMessage"
	params = {
		"chat_id": TELEGRAM_CHAT_ID,
		"text": message
	}
	response = requests.get(url, params=params)
	if response.status_code == 200:
		print(f"{get_timestamp()} [INFO] Mensaje enviado correctamente.")
	else:
		print(get_timestamp()+" [ERROR] Error al enviar el mensaje:", response.text)

# Llama a la función pasando la ruta del archivo de configuración de Zulip y el archivo de configuración de Telegram
try:
	forward_zulip_notifications_to_telegram()
except KeyboardInterrupt:
	# Acciones a realizar cuando se interrumpe la ejecución con Ctrl+C
	error_message = f"{get_timestamp()} [WARN] zulip2telegram.py interrumpido por el usuario."
	print(error_message)
	send_telegram_message(error_message)
	sys.exit(0)
except Exception as e:
	error_message = f"{get_timestamp()} [ERROR] Error inesperado en zulip2telegram.py: {str(e)}"
	print(error_message)
	send_telegram_message(error_message)
	sys.exit(1)